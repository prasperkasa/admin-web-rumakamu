var showPopupStatus = false;
var showPopupSecondStatus = false;


// GET CLASS
function getElement(attr){
  return document.querySelector(attr);
};

// GET CLASS
function getClass(attr){
  return document.getElementsByClassName(attr);
};

// GET ID
function getId(attr){
  return document.getElementById(attr);
};

// GET WINDOW HEIGHT
var windowHeight = getClass('window_height');
for (var i = 0; i < windowHeight.length; i++) {
  windowHeight[i].style.height=window.innerHeight;
};



function showPopup(){
	if(!showPopupStatus){
		getClass("ol-popup--general")[0].classList.add("show");
		getElement("body").classList.add("popup");
		showPopupStatus=true;
	}else{
		getClass("ol-popup--general")[0].classList.remove("show");
		getElement("body").classList.remove("popup");
		showPopupStatus=false;
	}
}

function showPopupSecond(){
	if(!showPopupSecondStatus){
		getClass("ol-popup--second")[0].classList.add("show");
		getElement("body").classList.add("popup");
		showPopupSecondStatus=true;
	}else{
		getClass("ol-popup--second")[0].classList.remove("show");
		getElement("body").classList.remove("popup");
		showPopupSecondStatus=false;
	}
}


function changeTable(self, val){
	var nav = getClass("ol-dashboard__content-form-list-item-nav-link");
	var spec = getClass("ol-dashboard__content-table-wrapper");
	for (var i = 0; i < spec.length; i++) {
		spec[i].classList.remove('show');
	 	nav[i].classList.remove('active');
	}
	self.classList.add('active');
	getId("table"+val).classList.add('show');
}


function back() {
  window.history.back();
}



function showSelectArea(areaHide, element)
{
    document.getElementById(areaHide).style.display = element.value == 2 ? 'inline-block' : 'none';
}

function showSelectRate(rateHide, element)
{
    document.getElementById(rateHide).style.display = element.value == 0 ? 'none' : 'inline-block';
    // document.getElementById(aveHide).style.display = element.value == 2 ? 'inline-block' : 'none';
}


//jquery

//SOFT LINK ONE PAGE
$(function() {  $('a[href*="#"]:not([href="#"])').click(function() {    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {      var target = $(this.hash);      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');      if (target.length) {        $('html, body').animate({          scrollTop: target.offset().top-70}, 1000);        return false;      }    }  });});




$(function() {
  $('#chgArea').change(function(){
    $('.ol-select-box--hide').hide();
    $('#' + $(this).val()).css({display:'inline-block'});
  });
});


$(function() {
  $('#chgCatBrand').change(function(){
    $('.ol-select-box--hide2').hide();
    $('#' + $(this).val()).css({display:'inline-block'});
  });
});







$(document).ready(function(){
	nav_cat_sub();
	data_table();
	data_table_popup();
	input_label();
	select_box();
	select_multiple();
	starReview();
	filter();
	prd_varian();
	prd_cat_sc();
	prd_cat();
	show_btn();
	back_top();
});


function nav_cat_sub(){

  $(".ol-dashboard-nav__list-item-link").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".ol-dashboard-nav__list-item-dd").hide(300);
    } else {
      $(".ol-dashboard-nav__list-item-link").removeClass("active");
      $(this).addClass("active");
      $(".ol-dashboard-nav__list-item-dd").hide(300);
      $(this).siblings(".ol-dashboard-nav__list-item-dd").show(300);
    }
  });

};


function input_label(){
	if( $('.ol-dashboard__content-form').length > 0 ) floatLabels();

	function floatLabels() {
		var inputFields = $('.ol-dashboard__content-form .ol-input-label').next();
		inputFields.each(function(){
			var singleInput = $(this);
			//check if user is filling one of the form fields 
			checkVal(singleInput);
			singleInput.on('change keyup', function(){
				checkVal(singleInput);	
			});
		});
	}

	function checkVal(inputField) {
		( inputField.val() == '' ) ? inputField.prev('.ol-input-label').removeClass('floating') : inputField.prev('.ol-input-label').addClass('floating');
	}
}

function data_table(){
	var oTable = $('.ol-dttable').DataTable();


	$('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = oTable.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );


    $("#chkAll").click(function (){
		$('.ol-table input:checkbox').not(this).prop('checked', this.checked);
	});
}

function data_table_popup(){
	var oTable = $('.ol-dttable--popup').DataTable(

	{
		columnDefs: [ {
	        orderable: false,
	        className: 'select-checkbox',
	        targets:   0
	    } ],
	    select: {
	        style:    'os',
	        selector: 'td:first-child'
	    },
	    order: [[ 1, 'asc' ]]
	}

	);

	$('#dtsearch').keyup(function(){
	      oTable.search($(this).val()).draw() ;
	})
}


function select_box(){
	$(".ol-input-select").select2({
	    // placeholder: "Select a state",
	    allowClear: true,
	    // minimumResultsForSearch: -1,
	    // placeholder: function(){
	    //     $(this).data('placeholder');
	    // }
	});


	$('#parentsku').select2({
	    placeholder: "Parent SKU",
	    allowClear: true,
	});

	$('#grupsku').select2({
	    placeholder: "SKU Group",
	    allowClear: true,
	});


	$('#category').select2({
	    placeholder: "Kategori",
	    allowClear: true,
	});

	$('#subcategory').select2({
	    placeholder: "Sub Kategori",
	    allowClear: true,
	});

	$('#typeproduct').select2({
	    placeholder: "Tipe Produk",
	    allowClear: true,
	});

	$('#color').select2({
	    placeholder: "Warna",
	    allowClear: true,
	});

	$('#brand').select2({
	    placeholder: "Brand",
	    allowClear: true,
	});

	$('#product').select2({
	    placeholder: "Pilih Produk",
	    allowClear: true,
	});

	$('#area').select2({
	    placeholder: "Area",
	    allowClear: true,
	});

	$('#status').select2({
	    placeholder: "Status",
	    allowClear: true,
	});
}

function select_multiple(){
	$(".ol-input--select-multiple").select2({
	    // placeholder: "Select a state",
	    // tags: true,
	    // minimumResultsForSearch: -1,
	    // placeholder: function(){
	    //     $(this).data('placeholder');
	    // }
	});
}


function starReview(){


	$('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    
  });
	

}


function filter(){
	$('.ol-dashboard__content-table-top-filter-btn').click(function(e){
		e.preventDefault();
		if($('.ol-dashboard__content-table-filter').hasClass('ol-dashboard__content-table-filter--show')){
			$('.ol-dashboard__content-table-filter').removeClass('ol-dashboard__content-table-filter--show');
		}else{
			$('.ol-dashboard__content-table-filter').addClass('ol-dashboard__content-table-filter--show');
		}
	})
};


function prd_varian(){

  $(".ol-dashboard__content-form-exp-item-title").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".ol-dashboard__content-form-exp-item-wrapper").hide();
    } else {
      $(".ol-dashboard__content-form-exp-item-title").removeClass("active");
      $(this).addClass("active");
      $(".ol-dashboard__content-form-exp-item-wrapper").hide();
      $(this).siblings(".ol-dashboard__content-form-exp-item-wrapper").show();
    }
  });

};


function prd_cat_sc(){
	if( $('.ol-dashboard__content-form-list-item').length > 0 ) floatLabels();

	function floatLabels() {
		var inputFields = $('.ol-dashboard__content-form-list-item').children().children();
		inputFields.each(function(){
			var singleInput = $(this);
			//check if user is filling one of the form fields 
			checkVal(singleInput);
			singleInput.on('change keyup', function(){
				checkVal(singleInput);	
			});
		});
	}

	function checkVal(inputField) {
		( inputField.val() == '' ) ? inputField.parent().next('.ol-dashboard__content-form-list-item-cat').slideUp(300) : inputField.parent().next('.ol-dashboard__content-form-list-item-cat').slideDown(300);
	}
}


function prd_cat(){

  $(".ol-dashboard__content-form-list-item-cat li").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).children().siblings(".ol-dashboard__content-form-list-item-cat-dd").hide();
    } else {
      $(".ol-dashboard__content-form-list-item-cat li").removeClass("active");
      $(this).addClass("active");
      $(".ol-dashboard__content-form-list-item-cat-dd").hide();
      $(this).children().siblings(".ol-dashboard__content-form-list-item-cat-dd").show();
    }
  });

};


function show_btn(){
	$('.ol-table input:checkbox').change(function () {

		var theoption = $('.ol-table input:checkbox');

        if (this.checked) 
           $('.ol-dashboard__content-table-check').show(300);

       		

        else 
            $('.ol-dashboard__content-table-check').hide(300);
    });
}


function back_top(){
	if ($('.ol-totop').length) {
		var scrollTrigger = 100, // px
			backToTop = function () {
				var scrollTop = $(window).scrollTop();
				if (scrollTop > scrollTrigger) {
					$('.ol-totop').addClass('show');
				} else {
					$('.ol-totop').removeClass('show');
				}
			};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		$('.ol-totop').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
	}
}